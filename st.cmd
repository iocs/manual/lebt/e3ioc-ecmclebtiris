##############################################################################
## EtherCAT Motion Control LEBT iris IOC configuration
##############################################################################
require essioc
require ecmccfg, 8.0.0

## Initiation:
epicsEnvSet("ECMC_VER" ,"8.0.0")
epicsEnvSet("IOC" ,"$(IOC="LEBT-Iris:Ctrl-ECAT-01")")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")

#- Choose motor record driver implementation
#-   ECMC_MR_MODULE="ecmcMotorRecord"  => ECMC native built in motor record support (Default)
#-   ECMC_MR_MODULE="EthercatMC"       => Motor record support from EthercatMC module (need to be loaded)
#- Uncomment the line below to use EthercatMC (and add optional EthercatMC_VER to startup.cmd call):
#- epicsEnvSet(ECMC_MR_MODULE,"EthercatMC")

# Epics Motor record driver that will be used:
# epicsEnvShow(ECMC_MR_MODULE)

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=8.0.0,stream_VER=2.8.22,MASTER_ID=0"

##############################################################################

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Configure hardware:

ecmcFileExist($(E3_CMD_TOP)/hw/ecmcMCULEBTiris.cmd,1)
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcMCULEBTiris.cmd

# Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

# ADDITIONAL SETUP
# Set all outputs to feed switches
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput01,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput02,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput03,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput04,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput05,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput06,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput07,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput08,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput09,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput10,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput11,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput12,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput13,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput14,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput15,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(4,binaryOutput16,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput01,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput02,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput03,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput04,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput05,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput06,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput07,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput08,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput09,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput10,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput11,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput12,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput13,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput14,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput15,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(5,binaryOutput16,1)"
# END of ADDITIONAL SETUP

##############################################################################
## Physical axes configuration

epicsEnvSet("DEV",      "LEBT-Iris:ID-Iris-01"
${SCRIPTEXEC} ${ecmccfg_DIR}configureAxis.cmd,            "CONFIG=$(E3_CMD_TOP)/cfg/mtr_1.pax"
${SCRIPTEXEC} ${ecmccfg_DIR}applyAxisSynchronization.cmd, "CONFIG=$(E3_CMD_TOP)/cfg/mtr_1.sax"
#
${SCRIPTEXEC} ${ecmccfg_DIR}configureAxis.cmd,            "CONFIG=$(E3_CMD_TOP)/cfg/mtr_2.pax"
${SCRIPTEXEC} ${ecmccfg_DIR}applyAxisSynchronization.cmd, "CONFIG=$(E3_CMD_TOP)/cfg/mtr_2.sax"
#
${SCRIPTEXEC} ${ecmccfg_DIR}configureAxis.cmd,            "CONFIG=$(E3_CMD_TOP)/cfg/mtr_3.pax"
${SCRIPTEXEC} ${ecmccfg_DIR}applyAxisSynchronization.cmd, "CONFIG=$(E3_CMD_TOP)/cfg/mtr_3.sax"
#
${SCRIPTEXEC} ${ecmccfg_DIR}configureAxis.cmd,            "CONFIG=$(E3_CMD_TOP)/cfg/mtr_4.pax"
${SCRIPTEXEC} ${ecmccfg_DIR}applyAxisSynchronization.cmd, "CONFIG=$(E3_CMD_TOP)/cfg/mtr_4.sax"
#
${SCRIPTEXEC} ${ecmccfg_DIR}configureAxis.cmd,            "CONFIG=$(E3_CMD_TOP)/cfg/mtr_5.pax"
${SCRIPTEXEC} ${ecmccfg_DIR}applyAxisSynchronization.cmd, "CONFIG=$(E3_CMD_TOP)/cfg/mtr_5.sax"
#
${SCRIPTEXEC} ${ecmccfg_DIR}configureAxis.cmd,            "CONFIG=$(E3_CMD_TOP)/cfg/mtr_6.pax"
${SCRIPTEXEC} ${ecmccfg_DIR}applyAxisSynchronization.cmd, "CONFIG=$(E3_CMD_TOP)/cfg/mtr_6.sax"

##############################################################################
## Virtual Axes configuration

#
${SCRIPTEXEC} ${ecmccfg_DIR}configureVirtualAxis.cmd,     "CONFIG=$(E3_CMD_TOP)/cfg/center_x.vax"
${SCRIPTEXEC} ${ecmccfg_DIR}applyAxisSynchronization.cmd, "CONFIG=$(E3_CMD_TOP)/cfg/center_x.sax"
#
${SCRIPTEXEC} ${ecmccfg_DIR}configureVirtualAxis.cmd,     "CONFIG=$(E3_CMD_TOP)/cfg/aperture.vax"
${SCRIPTEXEC} ${ecmccfg_DIR}applyAxisSynchronization.cmd, "CONFIG=$(E3_CMD_TOP)/cfg/aperture.sax"

${SCRIPTEXEC} ${ecmccfg_DIR}configureVirtualAxis.cmd,     "CONFIG=$(E3_CMD_TOP)/cfg/center_y.vax"
${SCRIPTEXEC} ${ecmccfg_DIR}applyAxisSynchronization.cmd, "CONFIG=$(E3_CMD_TOP)/cfg/center_y.sax"

##############################################################################

## PLC 0:
$(SCRIPTEXEC) $(ecmccfg_DIR)loadPLCFile.cmd, "PLC_ID=0, SAMPLE_RATE_MS=100,FILE=$(E3_CMD_TOP)/plc/movesync, PLC_MACROS='SAMPLE_RATE_MS=100,PLC_ID=0,DBG='")
dbLoadRecords("ecmcPlcBinary.db","P=$(IOC):,PORT=MC_CPU1,ASYN_NAME=plcs.plc0.static.movesync,REC_NAME=-MoveSync,TSE=0,T_SMP_MS=100")

## PLC 1:
$(SCRIPTEXEC) $(ecmccfg_DIR)loadPLCFile.cmd, "PLC_ID=1, SAMPLE_RATE_MS=100,FILE=$(E3_CMD_TOP)/plc/moveindep, PLC_MACROS='SAMPLE_RATE_MS=100,PLC_ID=1,DBG='")
dbLoadRecords("ecmcPlcBinary.db","P=$(IOC):,PORT=MC_CPU1,ASYN_NAME=plcs.plc1.static.moveindep,REC_NAME=-MoveIndep,TSE=0,T_SMP_MS=100")

dbLoadRecords("ecmcPlcBinary.db","P=$(IOC):,PORT=MC_CPU1,ASYN_NAME=plcs.global.readysync,REC_NAME=-ReadySync,TSE=0,T_SMP_MS=100")
dbLoadRecords("ecmcPlcBinary.db","P=$(IOC):,PORT=MC_CPU1,ASYN_NAME=plcs.global.readyindep,REC_NAME=-ReadyIndep,TSE=0,T_SMP_MS=100")

## PLC 2:
$(SCRIPTEXEC) $(ecmccfg_DIR)loadPLCFile.cmd, "PLC_ID=2, SAMPLE_RATE_MS=1000,FILE=$(E3_CMD_TOP)/plc/autodisable, PLC_MACROS='SAMPLE_RATE_MS=1000,PLC_ID=2,DBG='")

### PLC 3:
$(SCRIPTEXEC) $(ecmccfg_DIR)loadPLCFile.cmd, "PLC_ID=3, SAMPLE_RATE_MS=10,FILE=$(E3_CMD_TOP)/plc/collisionrisk, PLC_MACROS='SAMPLE_RATE_MS=10,PLC_ID=3,DBG='")
dbLoadRecords("ecmcPlcBinary.db","P=$(IOC):,PORT=MC_CPU1,ASYN_NAME=plcs.global.collisionrisk,REC_NAME=-CollisionRisk,TSE=0,T_SMP_MS=10"
dbLoadRecords("ecmcPlcBinary.db","P=$(IOC):,PORT=MC_CPU1,ASYN_NAME=plcs.global.collisionriskaxes14,REC_NAME=-CollRiskAx14,TSE=0,T_SMP_MS=10"
dbLoadRecords("ecmcPlcBinary.db","P=$(IOC):,PORT=MC_CPU1,ASYN_NAME=plcs.global.collisionriskaxes25,REC_NAME=-CollRiskAx25,TSE=0,T_SMP_MS=10"
dbLoadRecords("ecmcPlcBinary.db","P=$(IOC):,PORT=MC_CPU1,ASYN_NAME=plcs.global.collisionriskaxes36,REC_NAME=-CollRiskAx36,TSE=0,T_SMP_MS=10"

##############################################################################
############# Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"
ecmcConfigOrDie "Cfg.SetDiagAxisIndex(1)"
ecmcConfigOrDie "Cfg.SetDiagAxisFreq(2)"
ecmcConfigOrDie "Cfg.SetDiagAxisEnable(0)"

# Reduce the amount of output log in the IOC shell

asynSetTraceMask(MC_CPU1,-1,1)
asynSetTraceIOMask(MC_CPU1,-1,6)
asynSetTraceInfoMask(MC_CPU1,-1,1)

# go active
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)


iocInit()

