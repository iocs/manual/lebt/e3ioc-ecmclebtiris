#!/usr/bin/env python3

# Usage example "./high_friction_test_param.py 2"

import sys
import time
from datetime import datetime

import ecmcSlitDemoLib
import epics

motorid = int(sys.argv[1])

if motorid < 1 or motorid > 6:
    print("Wrong axis number")
    sys.exit()

P = "LEBT-Iris:ID-Iris-01:"

motor = P + "Mtr" + str(motorid)

if motorid < 4:
    oppositeid = motorid + 3
elif motorid < 7:
    oppositeid = motorid - 3

opposite = P + "Mtr" + str(oppositeid)

if motorid == 1 or motorid == 2 or motorid == 6:
    oppsafeswitch = opposite + ".LLS"
elif motorid == 3 or motorid == 4 or motorid == 5:
    oppsafeswitch = opposite + ".HLS"

logname = "axis" + str(motorid) + "test.log"

# print("motor ", motor, ", opposite ", opposite, ", oppsafeswitch ", oppsafeswitch, ", logname ", logname)

log = open(logname, "a+")

i = 1
while i < 7:
    if i != motorid:
        ecmcSlitDemoLib.setAxisEnable(P + "Mtr" + str(i), 0)
    #    print("Disabling motor num ", str(i))
    i = i + 1

time.sleep(0.5)

IsSafe = int(epics.caget(oppsafeswitch))
while IsSafe == 1:
    hlspressed = int(epics.caget(motor + ".HLS"))
    llspressed = int(epics.caget(motor + ".LLS"))
    if hlspressed:
        time.sleep(2)
        actpos = epics.caget(motor + "-PosAct")
        mov = actpos - 50
        print(
            datetime.now(), "HLS at %s, moving to %s" % (f"{actpos:.2f}", f"{mov:.2f}")
        )
        now = datetime.now()
        text = (
            now.strftime("%d/%m/%Y %H:%M:%S")
            + " HLS at "
            + f"{actpos:.2f}"
            + ", moving to "
            + f"{mov:.2f}"
            + "\n"
        )
        log.write(text)
        time.sleep(2)
        ecmcSlitDemoLib.moveAxisPosition(motor, mov)
        ecmcSlitDemoLib.waitForAxis(motor, 1800)
    time.sleep(2)
    if llspressed:
        time.sleep(2)
        actpos = epics.caget(motor + "-PosAct")
        mov = actpos + 50
        print(
            datetime.now(), "LLS at %s, moving to %s" % (f"{actpos:.2f}", f"{mov:.2f}")
        )
        now = datetime.now()
        text = (
            now.strftime("%d/%m/%Y %H:%M:%S")
            + " LLS at "
            + f"{actpos:.2f}"
            + ", moving to "
            + f"{mov:.2f}"
            + "\n"
        )
        log.write(text)
        time.sleep(2)
        ecmcSlitDemoLib.moveAxisPosition(motor, mov)
        ecmcSlitDemoLib.waitForAxis(motor, 1800)
    time.sleep(2)
    IsSafe = int(epics.caget(oppsafeswitch))
