############################################################
############# LEBT iris EtherCAT HW configuration

# 0   0:0   PREOP  +  EK1100 EtherCAT Coupler (2A E-Bus)
# 1   0:1   PREOP  +  EL1808 8K. Dig. Eingang 24V, 3ms
# 2   0:2   PREOP  +  EL1808 8K. Dig. Eingang 24V, 3ms
# 3   0:3   PREOP  +  EL1808 8K. Dig. Eingang 24V, 3ms
# 4   0:4   PREOP  +  EL2819 16K. Dig. Ausgang 24V, 0,5A, Diagnose
# 5   0:5   PREOP  +  EL2819 16K. Dig. Ausgang 24V, 0,5A, Diagnose
# 6   0:6   PREOP  +  EL9410 E-Bus Netzteilklemme (Diagnose)
# 7   0:7   PREOP  +  EL7041 1K. Schrittmotor-Endstufe (50V, 5A)
# 8   0:8   PREOP  +  EL7041 1K. Schrittmotor-Endstufe (50V, 5A)
# 9   0:9   PREOP  +  EL7041 1K. Schrittmotor-Endstufe (50V, 5A)
# 10  0:10  PREOP  +  EL7041 1K. Schrittmotor-Endstufe (50V, 5A)
# 11  0:11  PREOP  +  EL7041 1K. Schrittmotor-Endstufe (50V, 5A)
# 12  0:12  PREOP  +  EL7041 1K. Schrittmotor-Endstufe (50V, 5A)


#Configure EK1100 EtherCAT coupler
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=0, HW_DESC=EK1100"

#Configure EL1808 digital input terminal
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=1, HW_DESC=EL1808"

#Configure EL1808 digital input terminal
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=2, HW_DESC=EL1808"

#Configure EL1808 digital input terminal
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=3, HW_DESC=EL1808"

#Configure EL2819 digital output terminal
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=4, HW_DESC=EL2819"

#Configure EL2819 digital output terminal
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=5, HW_DESC=EL2819"

#Configure EL9410 Power supply with refresh of E-Bus.
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=6, HW_DESC=EL9410"

#Configure EL7041 stepper drive terminal, motor 1
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=7, HW_DESC=EL7041, CONFIG=-Motor-OwisLTM60"
ecmcConfigOrDie "Cfg.EcAddSdo(7,0x8010,0x1,1500,2)"

#Configure EL7041 stepper drive terminal, motor 2
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=8, HW_DESC=EL7041, CONFIG=-Motor-OwisLTM60"
ecmcConfigOrDie "Cfg.EcAddSdo(8,0x8010,0x1,1500,2)"

#Configure EL7041 stepper drive terminal, motor 3
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=9, HW_DESC=EL7041, CONFIG=-Motor-OwisLTM60"
ecmcConfigOrDie "Cfg.EcAddSdo(9,0x8010,0x1,1500,2)"

#Configure EL7041 stepper drive terminal, motor 4
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=10, HW_DESC=EL7041, CONFIG=-Motor-OwisLTM60"
ecmcConfigOrDie "Cfg.EcAddSdo(10,0x8010,0x1,1500,2)"

#Configure EL7041 stepper drive terminal, motor 5
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=11, HW_DESC=EL7041, CONFIG=-Motor-OwisLTM60"
ecmcConfigOrDie "Cfg.EcAddSdo(11,0x8010,0x1,1500,2)"

#Configure EL7041 stepper drive terminal, motor 6
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=12, HW_DESC=EL7041, CONFIG=-Motor-OwisLTM60"
ecmcConfigOrDie "Cfg.EcAddSdo(12,0x8010,0x1,1500,2)"

#Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

